#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import argparse
import csv
import json
import requests
import datetime

def get_token(auth_data):
    response = requests.post(url=args.api_url + '/auth', data=json.dumps(auth_data))
    if response.status_code == 200:
        return response.json()['token'], datetime.datetime.now()
    else:
        print('Credenziali non valide')
        exit(1)


if __name__ == "__main__":
    # Command arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("api_url")
    parser.add_argument("username")
    parser.add_argument("password")
    parser.add_argument("file")
    parser.add_argument("--delimiter", type=str, default=',', help="Delimitatore csv")
    parser.add_argument("--output-file", type=str, default="output.csv", help="Nome del file di output generato")
    parser.add_argument("--skip-rows", type=int, default=0, help="Numero di righe da saltare")
    parser.add_argument("--force-update", default=False, action='store_true', help="Forza l'aggiornamento dei file esistenti")
    args = parser.parse_args()

    # Auth credentials
    auth_data = {
        "username": args.username,
        "password": args.password
    }
    auth_header = {}
    folders = {}

    token, time = get_token(auth_data)
    auth_header["Authorization"] = "Bearer " + token

    output_file = open(args.output_file, mode='w')
    writer = csv.DictWriter(output_file, fieldnames=[
        'Folder Name *', 'Fiscal Code *', 'Mime-type *', 'Original Filename *', 'File Address *', 'Title *',
        'Store *', 'Description', 'Expire At', 'Validity Begin', 'Validity End', 'Document ID', 'Folder ID'
    ])
    writer.writeheader()
    output_file.flush()

    imported = 0
    modified = 0
    with open(args.file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=args.delimiter)

        # Skip rows
        for _ in range(args.skip_rows):
            next(csv_reader)

        for row in csv_reader:
            # Refresh token every 50 minutes
            if (datetime.datetime.now() - time).seconds > 60 * 50:
                token, time = get_token(auth_data)
                auth_header["Authorization"] = "Bearer " + token

            cf = row[1]
            print('Codice fiscale: ' + cf)
            qs = '?cf=' + cf
            response = requests.get(url=args.api_url + '/users' + qs, headers=auth_header)
            if response.status_code == 200 and response.json():
                user_id = response.json()[0]['id']
                print('Utente gia esistente: ' + user_id)
            elif response.status_code == 200:
                user_data = {"codice_fiscale": cf}
                response = requests.post(url=args.api_url + "/users", data=json.dumps(user_data), headers=auth_header)
                if response.status_code == 201:
                    user_id = response.json()['id']
                    print('Creato utente: ' + user_id)
                else:
                    print('Qualcosa � andato storto durante la creazione dell\'utente ' + response.text)
                    continue
            folder_id = row[12]
            if folder_id:
                # Controllo correttezza folder id
                response = requests.get(url=args.api_url + "/folders/" + folder_id, headers=auth_header)
                if response.status_code == 200:
                    print(folder_id + ' corretto')
                    # Controllo se � cambiato il nome della cartella
                    if row[0] != response.json()['title']:
                        # Verifico che non esista gi� una cartella con quel nome per lo stesso utente
                        qs = '?cf=' + cf + '&title=' + row[0]
                        response = requests.get(url=args.api_url + '/folders' + qs, headers=auth_header)
                        if response.status_code == 200 and response.json():
                            folder_id = response.json()[0]['id']
                            print('Cartella esistente: sposto il documento nella cartella ' + folder_id)
                        else:
                            folder_data_patch = {"title": row[0]}
                            response = requests.patch(
                                url=args.api_url + '/folders/' + folder_id,
                                data=json.dumps(folder_data_patch),
                                headers=auth_header
                            )
                            if response.status_code == 200:
                                print('Cartella modificata correttamente')
                            else:
                                print('Si � verificato un errore durante la modifica della cartella')
                                continue
                else:
                    print('Folder ID ' + folder_id + 'non corrisponde ad una cartella esistente')
                    continue
            else:
                # Ricerco cartella per titolo e codice fiscale proprietario
                qs = '?cf=' + cf + '&title=' + row[0]
                response = requests.get(url=args.api_url + '/folders' + qs, headers=auth_header)
                if response.status_code == 200 and response.json():
                    folder_id = response.json()[0]['id']
                    print('Cartella esistente: ' + folder_id)
                else:
                    print('Creo cartella')
                    folder_data = {
                        "owner": user_id,
                        "title": row[0]
                    }
                    response = requests.post(url=args.api_url + '/folders', data=json.dumps(folder_data),
                                             headers=auth_header)
                    if response.status_code == 201:
                        folder_id = response.json()['id']
                        print('Creata cartella: ' + folder_id)
                    else:
                        print('Qualcosa � andato storto durante la creazione della cartella: ' + response.text)
                        continue
            document_id = row[11]
            if document_id:
                # Check folder
                response = requests.get(url=args.api_url + "/documents/" + document_id, headers=auth_header)
                if response.status_code == 200:
                    print(response.json()['title'], row[5])
                    if response.json()['title'] != row[5]:
                        # Se il titolo � stato modificato, ma esiste gia un documento con quel nome nella stessa folder restituisco un errore
                        qs = '?cf=' + cf + '&folder=' + folder_id + '&title=' + row[5]
                        response = requests.get(url=args.api_url + '/documents' + qs, headers=auth_header)
                        if response.status_code == 200 and response.json():
                            print('Esiste gi� un documento con lo stesso nome con id: ' + response.json()[0]['id'])
                            continue
                    document_data = {
                        "owner": user_id,
                        "folder": folder_id,
                        "original_filename": row[3].encode('ascii', 'ignore').decode(),
                        "mime_type": row[2],
                        "address": row[4],
                        "store": row[6] == True if row[6] == '1' else False,
                        "title": row[5],
                        "description": row[7],
                        "validity_begin": row[9],
                        "validity_end": row[10],
                        "expire_at": row[8]
                    }
                    response = requests.put(
                        url=args.api_url + '/documents/' + document_id,
                        data=json.dumps(document_data),
                        headers=auth_header
                    )
                    if response.status_code == 200:
                        print('modificato documento: ' + document_id)
                        modified += 1
                        print('Documenti modificati: ' + str(modified))
                    else:
                        print('Qualcosa � andato storto durante la modifica del documento: ' + document_id)
                        continue
                else:
                    print('Document ID ' + document_id + 'non corrisponde ad un documento esistente')
                    continue
            else:
                qs = '?cf=' + cf + '&folder=' + folder_id + '&title=' + row[5].strip()
                response = requests.get(url=args.api_url + '/documents' + qs, headers=auth_header)
                if response.status_code == 200 and response.json():
                    document_id = response.json()[0]['id']
                    print('Esiste gi� un documento con lo stesso nome con id: ' + document_id)
                    if (args.force_update):
                        print("Forzo aggiornamento documento")
                        document_data = {
                            "owner": user_id,
                            "folder": folder_id,
                            "original_filename": row[3].encode('ascii', 'ignore').decode(),
                            "mime_type": row[2],
                            "address": row[4],
                            "store": row[6] == True if row[6] == '1' else False,
                            "title": row[5],
                            "description": row[7],
                            "validity_begin": row[9],
                            "validity_end": row[10],
                            "expire_at": row[8]
                        }
                        response = requests.put(
                            url=args.api_url + '/documents/' + document_id,
                            data=json.dumps(document_data),
                            headers=auth_header
                        )
                        if response.status_code == 200:
                            print('modificato documento: ' + document_id)
                            modified += 1
                            print('Documenti modificati: ' + str(modified))
                        else:
                            print('Qualcosa � andato storto durante la modifica del documento: ' + document_id)
                            continue
                else:
                    print('Creo documento')
                    document_data = {
                        "owner": user_id,
                        "folder": folder_id,
                        "original_filename": row[3].encode('ascii', 'ignore').decode(),
                        "mime_type": row[2],
                        "address": row[4],
                        "store": row[6] == True if row[6] == '1' else False,
                        "title": row[5],
                        "description": row[7],
                        "validity_begin": row[9],
                        "validity_end": row[10],
                        "expire_at": row[8]
                    }
                    response = requests.post(
                        url=args.api_url + '/documents',
                        data=json.dumps(document_data),
                        headers=auth_header
                    )
                    if response.status_code == 201:
                        document_id = response.json()['id']
                        print('creato documento: ' + document_id)
                        imported += 1
                        print('Documenti importati: ' + str(imported))
                    else:
                        print('Qualcosa � andato storto durante la creazione del documento: ' + response.text)
                        continue
            print('ok')
            outRow = {
                'Folder Name *': row[0],
                'Fiscal Code *': cf,
                'Mime-type *': row[2],
                'Original Filename *': row[3],
                'File Address *': row[4],
                'Title *': row[5],
                'Store *': row[6],
                'Description': row[7],
                'Expire At': row[8],
                'Validity Begin': row[9],
                'Validity End': row[10],
                'Document ID': document_id,
                'Folder ID': folder_id
            }
            writer.writerow(outRow)
            output_file.flush()
